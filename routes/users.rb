class SinatraApi < Sinatra::Application
  get "/users/?" do
    MultiJson.dump(User.all.map(&:to_api))
  end
end
