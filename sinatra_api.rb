require 'yaml'
require 'sinatra'
require "sinatra/json"
require "sequel"
require 'sinatra/reloader' if development?

# connect to db
Sequel.connect(YAML.load(File.read('./config/database.yml'))['development'])

# load all other files
%w{helpers models routes}.each {|dir| Dir.glob("./#{dir}/*.rb", &method(:require))}

class SinatraApi < Sinatra::Application
  # Application code

  run! if app_file == $0
end
