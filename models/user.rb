class User < Sequel::Model
  def to_api
    {
      id: id,
      name: name,
      address: address
    }
  end
end
